package uk.co.caeldev.stringandlist;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class Ex1Dot5Test {
    
    @Test
    public void shouldBeEqualByOneOperation() throws Exception {
        //Given
        String source1 = "pale";
        String source2 = "pal";
         
        //When
        boolean result = StringComparisionUtils.isEqualsByOneOperation(source1, source2);

        //Then
        assertThat(result).isTrue();
    }
}
