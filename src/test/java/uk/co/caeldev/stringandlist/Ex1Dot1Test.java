package uk.co.caeldev.stringandlist;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class Ex1Dot1Test {

    @Test
    public void shouldDetectStringWithUniqueChars() throws Exception {
        //Given
        String source = "abcd";

        //When
        final UniqueStringCharsDetector uniqueStringCharsDetector = new UniqueStringCharsDetector();
        boolean result = uniqueStringCharsDetector.detect(source);

        //Then
        assertThat(result).isTrue();
    }

    @Test
    public void shouldDetectStringWithNonUniqueChars() throws Exception {
        //Given
        String source = "abcda";

        //When
        final UniqueStringCharsDetector uniqueStringCharsDetector = new UniqueStringCharsDetector();
        boolean result = uniqueStringCharsDetector.detect(source);

        //Then
        assertThat(result).isFalse();
    }

    @Test
    public void shouldDetectStringWithUniqueCharsUsingNoAdditionalStructures() throws Exception {
        //Given
        String source = "abcd";

        //When
        final UniqueStringCharsDetector uniqueStringCharsDetector = new UniqueStringCharsDetector();
        boolean result = uniqueStringCharsDetector.detectV2(source);

        //Then
        assertThat(result).isTrue();
    }

    @Test
    public void shouldDetectStringWithNoUniqueCharsUsingNoAdditionalStructures() throws Exception {
        //Given
        String source = "abcda";

        //When
        final UniqueStringCharsDetector uniqueStringCharsDetector = new UniqueStringCharsDetector();
        boolean result = uniqueStringCharsDetector.detectV2(source);

        //Then
        assertThat(result).isFalse();
    }
}
