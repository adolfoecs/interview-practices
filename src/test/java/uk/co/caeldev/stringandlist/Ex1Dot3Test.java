package uk.co.caeldev.stringandlist;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class Ex1Dot3Test {

    @Test
    public void shouldReplaceSpacesBySpecialChar() throws Exception {
        //Given
        String source = "the most amazing day      ";
        int sourceLength = 20;

        //When
        final StringReplacer stringReplacer = new StringReplacer();
        String result = stringReplacer.replace(source, sourceLength, "%20");

        //Then
        assertThat(result).isEqualTo("the%20most%20amazing%20day");
    }
}
