package uk.co.caeldev.stringandlist;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class Ex1Dot4Test {
    @Test
    public void shouldDetectPalindromeWord() throws Exception {
        //Given
        String input = "Tact Coa";

        //When
        final PalindromeDetector palindromeDetector = new PalindromeDetector();
        boolean result = palindromeDetector.detect(input);

        //Then
        assertThat(result).isTrue();
    }

    @Test
    public void shouldDetectPalindromeWord2() throws Exception {
        //Given
        String input = "menem";

        //When
        final PalindromeDetector palindromeDetector = new PalindromeDetector();
        boolean result = palindromeDetector.detect(input);

        //Then
        assertThat(result).isTrue();
    }

    @Test
    public void shouldDetectPalindromeWord3() throws Exception {
        //Given
        String input = "menem menem b";

        //When
        final PalindromeDetector palindromeDetector = new PalindromeDetector();
        boolean result = palindromeDetector.detect(input);

        //Then
        assertThat(result).isTrue();
    }
}
