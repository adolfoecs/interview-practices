package uk.co.caeldev.stringandlist;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class Ex1Dot2Test {

    @Test
    public void shouldDetectPermutation() throws Exception {
        //Given
        String permutation1 = "abc";
        String permutation2 = "cba";

        //When
        final PermutationDetector permutationDetector = new PermutationDetector();
        boolean result = permutationDetector.detect(permutation1, permutation2);

        //Then
        assertThat(result).isTrue();

    }

    @Test
    public void shouldDetectNoPermutation() throws Exception {
        //Given
        String permutation1 = "abr";
        String permutation2 = "cba";

        //When
        final PermutationDetector permutationDetector = new PermutationDetector();
        boolean result = permutationDetector.detect(permutation1, permutation2);

        //Then
        assertThat(result).isFalse();

    }

    @Test
    public void shouldDetectPermutationV2() throws Exception {
        //Given
        String permutation1 = "abc";
        String permutation2 = "cba";

        //When
        final PermutationDetector permutationDetector = new PermutationDetector();
        boolean result = permutationDetector.detectV2(permutation1, permutation2);

        //Then
        assertThat(result).isTrue();

    }

    @Test
    public void shouldDetectNoPermutationV2() throws Exception {
        //Given
        String permutation1 = "abr";
        String permutation2 = "cab";

        //When
        final PermutationDetector permutationDetector = new PermutationDetector();
        boolean result = permutationDetector.detectV2(permutation1, permutation2);

        //Then
        assertThat(result).isFalse();

    }
}
