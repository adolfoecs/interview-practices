package uk.co.caeldev.stringandlist;

public class StringReplacer {

    public String replace(String source, int sourceLength, String replace) {
        char[] chars = new char[source.length()];
        int lastCharAtNewArray = 0;
        for (int i = 0; i < sourceLength; i++) {
            if (source.charAt(i) != ' ') {
                chars[lastCharAtNewArray] = source.charAt(i);
                lastCharAtNewArray++;
            } else {
                chars[lastCharAtNewArray] = '%';
                chars[lastCharAtNewArray+1] = '2';
                chars[lastCharAtNewArray+2] = '0';
                lastCharAtNewArray = lastCharAtNewArray + 3;
            }
        }

        return new String(chars);
    }
}
