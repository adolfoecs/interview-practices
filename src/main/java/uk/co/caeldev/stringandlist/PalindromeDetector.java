package uk.co.caeldev.stringandlist;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class PalindromeDetector {

    public boolean detect(String input) {
        Map<String, Integer> leftSide = new HashMap<>();
        Map<String, Integer> rightSide = new HashMap<>();
        for (int i = 0; i < input.length(); i++) {
            final Character it = input.charAt(i);
            final String key = String.valueOf(it).toLowerCase();

            if (" ".equals(key)) {
                continue;
            }

            if (leftSide.get(key) == null) {
                leftSide.put(key, 1);
                continue;
            }

            if (rightSide.get(key) == null) {
                rightSide.put(key, 1);
                continue;
            }

            if (leftSide.get(key) < rightSide.get(key)) {
                leftSide.put(key, leftSide.get(key) + 1);
            } else {
                rightSide.put(key, rightSide.get(key) + 1);
            }
        }

        int middleChar = 0;

        for (Entry<String, Integer> entry : leftSide.entrySet()) {
            final String leftKey = entry.getKey();
            final Integer leftValue = entry.getValue();
            final Integer rightValue = rightSide.get(leftKey);
            if (rightValue == null) {
                middleChar++;
                continue;
            }

            if (leftValue != rightValue) {
                middleChar++;
            }
        }

        return middleChar < 2;
    }
}
