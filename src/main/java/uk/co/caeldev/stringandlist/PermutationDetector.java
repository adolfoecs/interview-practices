package uk.co.caeldev.stringandlist;

import java.util.LinkedList;

public class PermutationDetector {

    public boolean detect(String permutation1, String permutation2) {
        final char[] chars1 = permutation1.toCharArray();
        final char[] chars2 = permutation2.toCharArray();

        if (chars1.length != chars2.length) {
            return false;
        }

        LinkedList<String> resources = new LinkedList<>();
        for (char c : chars1) {
            resources.add(String.valueOf(c));
        }

        for (char c : chars2) {
            resources.remove(String.valueOf(c));
        }

        return resources.isEmpty();
    }

    public boolean detectV2(String permutation1, String permutation2) {
        final char[] chars1 = permutation1.toCharArray();
        char[] chars2 = permutation2.toCharArray();

        if (chars1.length != chars2.length) {
            return false;
        }

        StringBuilder sb = new StringBuilder(permutation2);

        for (char c : chars1) {
            for (int i = 0; i < chars2.length; i++) {
                if (chars2[i] == c) {
                    sb.deleteCharAt(i);
                    break;
                }
            }
            chars2 = sb.toString().toCharArray();
        }

        return chars2.length == 0;
    }


}
