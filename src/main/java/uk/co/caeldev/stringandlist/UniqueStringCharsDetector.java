package uk.co.caeldev.stringandlist;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

public class UniqueStringCharsDetector {

    public boolean detect(String source) {
        final char[] chars = source.toCharArray();
        Set<Character> counter = new HashSet<>();
        for (char aChar : chars) {
            counter.add(aChar);
        }

        return counter.size() == chars.length? true:false;
    }

    public boolean detectV2(String source) {
        CustomHashTable customMap = new CustomHashTable();

        final char[] chars = source.toCharArray();

        for (char aChar : chars) {
            final String key = String.valueOf(aChar);
            if (customMap.get(key) == null) {
                customMap.put(key, key);
            } else {
                return false;
            }
        }
        return true;
    }

    class CustomHashTable {

        LinkedList<CustomKeyValueEntry>[] customHastTable = new LinkedList[200];

        public void put(String key, String value) {

            LinkedList<CustomKeyValueEntry> entry = customHastTable[key.hashCode()];

            if (entry == null) {
                customHastTable[key.hashCode()] = new LinkedList<>();
                entry = customHastTable[key.hashCode()];
            }

            entry.add(new CustomKeyValueEntry(key, value));
        }

        public String get(String key) {
            final LinkedList<CustomKeyValueEntry> entries = customHastTable[key.hashCode()];

            if (entries == null) {
                return null;
            }

            for (CustomKeyValueEntry entry : entries) {
                if (entry.getKey().equals(key)) {
                    return key;
                }
            }

            return null;
        }
    }

    class CustomKeyValueEntry {
        private String key;
        private String value;

        public CustomKeyValueEntry(String key, String value) {
            this.key = key;
            this.value = value;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }
}
